'use strict';

angular.module('votekApp')
  .controller('TreeCtrl', function($scope, $http, $location, mainService, $routeParams) {
    console.log('$routeParams', $routeParams);
    var tree = $routeParams.tree;
    $scope.tree = {};
    mainService.getTree({
      tree: tree
    }).then(function(tree) {
      $scope.tree = tree.data
      $scope.treeData = convertJsonToD3Format(tree.data);
      console.log('$scope.treeData', $scope.treeData);
    }, function(tree) {
      alert('error fetching tree');
      console.error('error fetching tree', tree);
    });


    $scope.encodeJson = function(data) {
      return JSON.stringify(data);
    }

    function convertJsonToD3Format(tree) {
      //root of the tree
      var parent = {
        id: tree.id,
        name: tree.tree_title,
        children: []
      }

      for (var i = 0; i < tree.tree_nodes.length; i++) {
        var node = tree.tree_nodes[i];
        var order = node.title.split('-'); // node order is based on its title
        var level = order.length - 1;

        //find node's father and push it to father's children array
        var nodeParent = parent //initially the parent is the original one
        for (var j = 0; j < level - 1; j++) {
          var parentIdx = parseInt(order[j + 1] - 1); //current node index in its parent children array
          nodeParent = nodeParent.children[parentIdx]
        }
        nodeParent.children.push(reformNode(node))
      }
      console.log('the tree', parent);
      return [parent];
    }

    //converting object structure to match d3 structure
    function reformNode(node) {
      return {
        id: node.id,
        name: node.title,
        children: []
      }
    }
    $scope.moveHome = function() {
      $location.path('/');
    }
  });
