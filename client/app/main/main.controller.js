'use strict';

angular.module('votekApp')
  .controller('MainCtrl', function($scope, $http, $location, mainService) {
    $scope.trees = [];
    mainService.getAllTrees().then(function(trees) {
      $scope.trees = trees.data;
      console.log('trees', trees.data);
    },function () {
      alert('error fetching trees');
    });


    /**
    * import tree by uploading a json file
    */
    $scope.onFileChange = function(fileEl) {
      var files = fileEl.files;
      var file = files[0];
      var reader = new FileReader();

      reader.onloadend = function(evt) {
        if (evt.target.readyState === FileReader.DONE) {
          $scope.$apply(function() {
            var newTreeObj = JSON.parse(evt.target.result);

            //push tree object to back-end
            mainService.newTree(newTreeObj)
              .then(function(tree) {
                $scope.trees.push(tree.data[tree.data.length]);
              }, function (tree) {
                alert('error importing tree')
              })

            $scope.trees.push(JSON.parse(evt.target.result))

          });
        }
      };

      reader.readAsText(file);
    };

    $scope.moveTo = function (id) {
      $location.path('/' + id);
    }
  });
