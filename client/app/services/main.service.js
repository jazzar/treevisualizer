(function () {

  angular
    .module('votekApp')
    .service('mainService', ['$http', '$window','$location', mainService]);

  function mainService ($http, $window,$location) {


    /**
    * GET all trees stored
    */
    var getAllTrees = function() {
      return $http({
        method: 'GET',
        url: '/api/trees',
      });
    };

    /**
    * GET  one tree
    */
    var getTree = function(data) {
      return $http({
        method: 'GET',
        url: '/api/trees/' + data.tree,
      });
    };

    /**
    * POST new tree
    */
    var newTree = function(data) {
      console.log('data is service', data);
      return $http({
        method: 'POST',
        url: '/api/trees/',
        data : data
      });
    };



    return {
      getAllTrees : getAllTrees,
      getTree : getTree,
      newTree : newTree
    };
  }


})();
