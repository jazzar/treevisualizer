'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TreeSchema = new Schema({

  tree_title: { type : String , unique : true},
  tree_nodes: [{
    id:String,
    title:String,
    previous_sibling_id:String,
    next_node_id:String
  }]
});

module.exports = mongoose.model('Tree', TreeSchema);
