/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /trees              ->  index
 * POST    /trees              ->  create
 * GET     /trees/:id          ->  show
 */

'use strict';

var _ = require('lodash');
var Tree = require('./tree.model');
var ObjectId = require('mongodb').ObjectID;

// Get list of trees
exports.index = function(req, res) {
  Tree.find(function(err, trees) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(200).json(trees);
  });
};

// Get a single tree
exports.show = function(req, res) {
  console.log('req of treee',req.params.id);
  Tree.findOne ( { 'tree_title' : req.params.id} , function(err, tree) {

    if (err) {
      return handleError(res, err);
    }
    if (!tree) {
      return res.status(404).send('Not Found');
    }
    return res.json(tree);
  });
};

// Creates a new tree in the DB.
exports.create = function(req, res) {
  Tree.create(req.body, function(err, tree) {
    if (err) {
      return handleError(res, err);
    }
    return res.status(201).json(tree);
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
